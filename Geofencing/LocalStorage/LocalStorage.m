//
//  LocalStorage.m
//  geofencing
//
//  Created by Mac on 16/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalStorage.h"

@implementation LocalStorage {
    NSUserDefaults *userDefaults;
    NSString *authTokenKey;
    NSString *notificationPermissionKey;
    NSString *firebaseInstanceIdKey;
    NSString *lastLocationKey;
    NSString *baseUrlKey;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (instancetype) init {
    self = [super init];
    authTokenKey = @"authToken";
    notificationPermissionKey = @"notificationPermission";
    firebaseInstanceIdKey = @"firebaseInstanceId";
    baseUrlKey = @"baseUrl";
    userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"PEEKABOO"];
    return self;
}

-(NSString *)getData:(NSString *)key {
    return [self->userDefaults stringForKey:key];
}

-(void)setData:(NSString *)key value:(NSString *)value {
    [self->userDefaults setObject:value forKey:key];
    [self->userDefaults synchronize];
}

-(void)setAuthToken:(NSString *)token {
    [self setData:authTokenKey value:token];
}

-(NSString *)getAuthToken {
    return [self getData:authTokenKey];
}


-(void)setNotificationPermission:(BOOL)granted {
    if (granted) {
        [self setData:notificationPermissionKey value:@"123"];
        return;
    }
    [self setData:notificationPermissionKey value:@"321"];
}

-(BOOL)getNotificationPermission {
    return [[self getData:notificationPermissionKey] isEqual:@"123"];
}


-(void)setInstanceId:(NSString *)fid {
    [self setData:firebaseInstanceIdKey value:fid];
}

-(NSString *)getInstanceId {
    if ([self getData:firebaseInstanceIdKey] != nil) {
        return [self getData:firebaseInstanceIdKey];
    }
    return @"";
}

-(void)setLastLocation:(NSDictionary *)location {
    [self setData:lastLocationKey value:@"location"];
}

-(NSString *)getLastLocation {
    return [self getData:lastLocationKey];
}


-(void)setBaseUrl:(NSString *)url {
    [self setData:baseUrlKey value:url];

}
-(NSString *)getBaseUrl {
    if ([self getData:baseUrlKey] != nil) {
        return [self getData:baseUrlKey];
    }
    return @"";
}

@end
