//
//  LocalStorage.h
//  geofencing
//
//  Created by Mac on 16/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef LocalStorage_h
#define LocalStorage_h

@interface LocalStorage : NSObject

+(id)sharedManager;

-(void)setAuthToken:(NSString *)token;
-(NSString *)getAuthToken;

-(void)setNotificationPermission:(BOOL)granted;
-(BOOL)getNotificationPermission;

-(void)setInstanceId:(NSString *)fid;
-(NSString *)getInstanceId;

-(void)setBaseUrl:(NSString *)url;
-(NSString *)getBaseUrl;

@end



#endif /* LocalStorage_h */
