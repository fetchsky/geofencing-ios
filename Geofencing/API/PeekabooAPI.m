//
//  PeekabooAPI.m
//  LocationServiceTesting
//
//  Created by Mac on 13/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Utils.h"
#import "DeviceConfigs.h"
#import "ApplicationConfigs.h"
#import "BackgroundScheduler.h"
#import "LocalStorage.h"
#import "LocationManager.h"

#import "PeekabooAPI.h"

@implementation PeekabooAPI {
    NSString *authToken;
    NSURLSession *defaultSession;
    NSDictionary *commonParams;
    BOOL isProcessing;
    BOOL isInstanceIdProcessing;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (instancetype) init {
    self = [super init];
    defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    authToken = [LocalStorage.sharedManager getAuthToken];
    return self;
}


-(void)setAuthToken:(NSString *)token {
    if (token == nil) {
        return;
    }
    authToken = [@"Bearer " stringByAppendingString:token];
    [LocalStorage.sharedManager setAuthToken:authToken];
}

-(NSDictionary *)getCommonBodyParams {
    if (!commonParams) {
        commonParams = @{
            @"timeZone": [DeviceConfigs.sharedManager getTimeZone],
            @"platform": @"IOS",
            @"platformVersion": [DeviceConfigs.sharedManager getPlatformModel],
            @"uuid": [DeviceConfigs.sharedManager getDeviceId],
            @"recipientId": [ApplicationConfigs.sharedManager getInstanceId],
        };
    }
    return commonParams;
}
    
-(NSData *)getJSONData:(NSDictionary *)parameters {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
    if (!jsonData || error != nil) {
        NSLog(@"Got an error: %@", error);
        return nil;
    }
    return jsonData;
}
    
-(NSMutableURLRequest *)getRequest:(NSString *)url
                            method:(NSString *)method
                   isAuthenticated:(BOOL)auth {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[[ApplicationConfigs.sharedManager getBaseUrl] stringByAppendingString:url]]];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (auth) {
        [request setValue:authToken forHTTPHeaderField:@"Authorization"];
    }
    return request;
}

-(void)registerDeviceAndSendLocation:(NSDictionary *)location
{
    NSError *error;
    NSMutableURLRequest *request = [self getRequest:@"/api/v1/device/register"
                                             method:@"POST"
                                    isAuthenticated:false];
    
    NSDictionary *body = [self getCommonBodyParams];
    NSData *jsonData = [self getJSONData:body];
    
    if (!jsonData || error != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Got an error: %@", error);
        });
        return;
    }
    
    [request setHTTPBody:jsonData];
    
    NSURLSessionDataTask *dataTask =
        [defaultSession
         dataTaskWithRequest:request
         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (data) {
                NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@", responseObject);
                });
                [self setAuthToken:[responseObject valueForKeyPath:@"data.token"]];
                [self sendLocationToServer:location];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"network error, %@", [error localizedFailureReason]);
                });
            }
    }];
    [dataTask resume];
}

-(void)registerDeviceAndSendInstanceId
{
    NSError *error;
    NSMutableURLRequest *request = [self getRequest:@"/api/v1/device/register"
                                             method:@"POST"
                                    isAuthenticated:false];
    
    NSDictionary *body = [self getCommonBodyParams];
    NSData *jsonData = [self getJSONData:body];
    
    if (!jsonData || error != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Got an error: %@", error);
        });
        return;
    }
    
    [request setHTTPBody:jsonData];
    
    NSURLSessionDataTask *dataTask =
        [defaultSession
         dataTaskWithRequest:request
         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (data) {
                NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@", responseObject);
                });
                [self setAuthToken:[responseObject valueForKeyPath:@"data.token"]];
                [self sendInstanceId];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"network error, %@", [error localizedFailureReason]);
                });
            }
    }];
    [dataTask resume];
}



-(void)sendLocationToServer:(NSDictionary *)location
{
    NSError *error;
    NSMutableURLRequest *request = [self getRequest:@"/api/v1/device/track"
                                             method:@"POST"
                                    isAuthenticated:true];
    
//    BOOL dwellIntervalLapsed = [[location valueForKey:@"dwellIntervalLapsed"] boolValue];
    
    NSMutableDictionary *body = [[NSMutableDictionary alloc] initWithDictionary:[self getCommonBodyParams]];
    [body setValue:@YES forKey:@"dwellIntervalLapsed"];
    [body setValue:[location valueForKey:@"latitude"] forKey:@"latitude"];
    [body setValue:[location valueForKey:@"longitude"] forKey:@"longitude"];

    NSData *jsonData = [self getJSONData:body];
    if (!jsonData || error != nil) {
        NSLog(@"Got an error: %@", error);
        return;
    }

    [request setHTTPBody:jsonData];
    NSLog(@"sendLocationToServer: %@", jsonData);
    NSURLSessionDataTask *dataTask = [defaultSession
        dataTaskWithRequest:request
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         if (data) {
             @try {
                 NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"%@", responseObject);
                 });
                 float proximity = [[responseObject valueForKeyPath:@"data.proximity"] doubleValue];
                 [LocationManager.sharedManager setDistanceFilter:proximity];
//                 if ([ApplicationConfigs.sharedManager showLocationLocalNotification]) {
//                     [BackgroundScheduler.sharedManager sendLocationLocalNotification:[Utils getLocalNotificationBody:body]];
//                 }
//                 if (!dwellIntervalLapsed) {
//                     NSMutableDictionary *geofenceParams = [[NSMutableDictionary alloc] initWithDictionary:body];
//                     double dwellInterval = [[responseObject valueForKeyPath:@"data.dwellInterval"] floatValue] * 60;
//                     [geofenceParams setValue:[NSString stringWithFormat:@"%f", proximity] forKey:@"radius"];
//                     [geofenceParams setValue:[NSString stringWithFormat:@"%f", dwellInterval] forKey:@"dwellInterval"];
//                     [BackgroundScheduler.sharedManager scheduleDwellInterval:geofenceParams];
//                 }
             }
             @catch (NSException *exception) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"network error, %@", exception.debugDescription);
                 });
             } @finally {
                 self->isProcessing = false;
             }
         }
         else {
             self->isProcessing = false;
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"network error, %@", [error localizedFailureReason]);
             });
         }
    }];
    [dataTask resume];
}



-(void)sendInstanceId
{
    NSError *error;
    NSMutableURLRequest *request = [self getRequest:@"/api/v1/device/refresh"
                                             method:@"POST"
                                    isAuthenticated:true];
        
    NSMutableDictionary *body = [[NSMutableDictionary alloc] initWithDictionary:[self getCommonBodyParams]];
    NSData *jsonData = [self getJSONData:body];
    if (!jsonData || error != nil) {
        NSLog(@"Got an error: %@", error);
        return;
    }

    [request setHTTPBody:jsonData];
    NSLog(@"sendInstanceId: %@", jsonData);
    NSURLSessionDataTask *dataTask = [defaultSession
        dataTaskWithRequest:request
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         if (data) {
             @try {
                 NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"%@", responseObject);
                 });
             }
             @catch (NSException *exception) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"network error, %@", exception.debugDescription);
                 });
             } @finally {
                 self->isInstanceIdProcessing = false;
             }
         }
         else {
             self->isInstanceIdProcessing = false;
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"network error, %@", [error localizedFailureReason]);
             });
         }
    }];
    [dataTask resume];
}


-(void)postLocation:(NSDictionary *)params {
    if (isProcessing) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"iP");
        });
        return;
    }
    isProcessing = true;
    if (!authToken) {
        [self registerDeviceAndSendLocation:params];
        return;
    }
    [self sendLocationToServer:params];
}

-(void)postInstanceId {
    if (isInstanceIdProcessing) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"iP");
        });
        return;
    }
    isInstanceIdProcessing = true;
    if (!authToken) {
        [self registerDeviceAndSendInstanceId];
        return;
    }
    [self sendInstanceId];
}

    
-(void)postNotificationStatus:(NSDictionary *)params {
    NSMutableURLRequest *request = [self getRequest:@"/api/v1/notification/status/update"
                                             method:@"POST"
                                    isAuthenticated:true];
    
    NSData *jsonData = [self getJSONData:@{
                                           @"notificationIdentifier": [params valueForKey:@"notificationIdentifier"],
                                           @"status": [params valueForKey:@"status"],
                                           }];
    if(!jsonData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Got an error");
        });
        return;
    }
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *dataTask = [defaultSession
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if (data) {
                                              NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              [Utils log:[[NSString alloc] initWithFormat:@"%@", responseObject]];
                                          }
                                          else {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [Utils log:[[NSString alloc] initWithFormat:@"network error, %@", [error localizedFailureReason]]];
                                              });
                                          }
                                      }];
    [dataTask resume];
}

@end
