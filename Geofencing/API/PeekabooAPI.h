//
//  PeekabooAPI.h
//  LocationServiceTesting
//
//  Created by Mac on 13/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef PeekabooAPI_h
#define PeekabooAPI_h

@interface PeekabooAPI : NSObject

+(id)sharedManager;
-(void)postLocation:(NSDictionary *)options;
-(void)postNotificationStatus:(NSDictionary *)params;
-(void)postInstanceId;

@end


#endif /* PeekabooAPI_h */
