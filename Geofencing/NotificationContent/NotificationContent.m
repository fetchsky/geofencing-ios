//
//  NotificationContent.m
//  Geofencing
//
//  Created by Mohammad Zain on 01/11/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationContent.h"
#import <UserNotifications/UserNotifications.h>

@implementation NotificationContent

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

-(void)initialize {
    UNNotificationAction *viewAction = [UNNotificationAction actionWithIdentifier:@"view" title:@"View" options:0];
    UNNotificationAction *navigateAction = [UNNotificationAction actionWithIdentifier:@"navigate" title:@"Navigate" options:0];
    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"pkb_deal_navigate"
                                                                              actions:@[viewAction, navigateAction]
                                                                    intentIdentifiers:@[]
                                                                              options:UNNotificationCategoryOptionCustomDismissAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center setNotificationCategories:[NSSet setWithObject:category]];
    });
}

@end

