//
//  NotificationContent.h
//  Geofencing
//
//  Created by Mohammad Zain on 01/11/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef NotificationContent_h
#define NotificationContent_h


@interface NotificationContent : NSObject

+(id)sharedManager;

-(void)initialize;

@end


#endif /* NotificationContent_h */
