//
//  DeviceConfigs.m
//  Geofencing
//
//  Created by Mohammad Zain on 24/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DeviceConfigs.h"
#import "DeviceUID.h"

@implementation DeviceConfigs {
    NSString* timeZone;
    NSString* deviceId;
    NSString* platformModel;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (NSString *) getDeviceId {
    if (!deviceId) {
//        deviceId = [[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
        deviceId = [DeviceUID uid];
    }
    return deviceId;

}

- (NSString *) getTimeZone {
    if (!timeZone) {
        NSDate *sourceDate = [NSDate dateWithTimeIntervalSinceNow:3600 * 24 * 60];
        NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
        float timeZoneOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate] / 3600.0;
        timeZone = [NSString stringWithFormat:@"%f", timeZoneOffset];
    }
    return timeZone;
};

-(NSString *) getPlatformModel{
    if (!platformModel) {
        platformModel = [[UIDevice currentDevice] systemVersion];
    }
    return platformModel;
}

@end
