//
//  DeviceConfigs.h
//  Geofencing
//
//  Created by Mohammad Zain on 24/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef DeviceConfigs_h
#define DeviceConfigs_h

@interface DeviceConfigs : NSObject

+(id)sharedManager;

- (NSString *) getDeviceId;
- (NSString *) getTimeZone;
- (NSString *) getPlatformModel;

@end


#endif /* DeviceConfigs_h */
