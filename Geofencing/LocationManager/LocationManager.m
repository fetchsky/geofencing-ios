//
//  LocationManager.m
//  LocationServiceTesting
//
//  Created by Mac on 02/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
@import Foundation;
@import CoreLocation;

#import "LocationManager.h"
#import "Utils.h"
#import "PeekabooAPI.h"
#import "BackgroundScheduler.h"
#import "NotificationManager.h"

@implementation LocationManager {
    CLLocationManager *locationManager;
}

//Class method to make sure the share model is synch across the app
+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (void)initializeLocationManager {
    if (locationManager != nil) {
        return;
    }
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.activityType = CLActivityTypeOtherNavigation;
    locationManager.distanceFilter = 50.f;
    if([locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
        [locationManager setAllowsBackgroundLocationUpdates:true];
    }
    [locationManager requestAlwaysAuthorization];
}

- (void)startMonitoringLocation {
    [self initializeLocationManager];
    [Utils log:@"LocationManager: startMonitoringLocation"];
//    [locationManager startUpdatingLocation];
    [locationManager startMonitoringSignificantLocationChanges];
}

-(void)startMonitoringSignificantLocationChange  {
    [self initializeLocationManager];
    [Utils log:@"LocationManager: startMonitoringSignificantLocationChange"];
    [locationManager startMonitoringSignificantLocationChanges];
}

- (void)restartMonitoringLocation {
    [self initializeLocationManager];
    [Utils log:@"LocationManager: restartMonitoringLocation"];
    [locationManager stopMonitoringSignificantLocationChanges];
    [locationManager requestAlwaysAuthorization];
}

- (void)setDistanceFilter:(double)distanceFilter {
    [Utils log:[[NSString alloc] initWithFormat:@"LocationManager: New Distance filter %f", distanceFilter]];
    locationManager.distanceFilter = distanceFilter;
    [locationManager startMonitoringSignificantLocationChanges];
//    [locationManager startUpdatingLocation];
}
    
    
#pragma mark - CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    @try {
        CLLocation *location = locations.lastObject;
        NSDictionary *params = @{
            @"latitude": [NSNumber numberWithFloat:location.coordinate.latitude],
            @"longitude": [NSNumber numberWithFloat:location.coordinate.longitude],
            @"horizontalAccuracy":[NSNumber numberWithFloat:location.horizontalAccuracy],
        };
        [Utils
         log:[[NSString alloc] initWithFormat:@"didUpdateLocations, %@, %@",
              [params valueForKey:@"latitude"], [params valueForKey:@"longitude"]]];
        [BackgroundScheduler.sharedManager cancelNotification];
        [PeekabooAPI.sharedManager postLocation:params];
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"error, %@", exception.debugDescription);
        });
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [locationManager startMonitoringVisits];
            break;
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            [NotificationManager.sharedManager setFirebaseInstanceId];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [locationManager requestAlwaysAuthorization];
            break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit {
    /**
     *  Store the visit event into the database so we can plot it later
     */
    NSLog(@"visit.arrivalDate: %@", visit.arrivalDate);
    NSLog(@"visit.departureDate: %@", visit.departureDate);
    NSLog(@"visit.coordinate.latitude: %f", visit.coordinate.latitude);
    NSLog(@"visit.coordinate.longitude: %f", visit.coordinate.longitude);
}


@end
