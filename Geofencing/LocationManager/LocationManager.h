//
//  LocationManager.h
//  LocationServiceTesting
//
//  Created by Mac on 02/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef LocationManager_h
#define LocationManager_h
@import Foundation;
@import CoreLocation;

@interface LocationManager : NSObject <CLLocationManagerDelegate>
        
+ (id)sharedManager;

- (void)initializeLocationManager;
- (void)setDistanceFilter:(double)filter;
- (void)startMonitoringLocation;
- (void)startMonitoringSignificantLocationChange;
- (void)restartMonitoringLocation;

@end
#endif /* LocationManager_h */
