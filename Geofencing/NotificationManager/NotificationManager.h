//
//  NotificationManager.h
//  LocationServiceTesting
//
//  Created by Mac on 07/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef NotificationManager_h
#define NotificationManager_h
@import Foundation;

@interface NotificationManager : NSObject

+ (id)sharedManager;
- (void)onNotificationReceived:(NSDictionary *)notification;
- (void)onNotificationOpened:(NSDictionary *)notification;
- (void)setFirebaseInstanceId;

@end

#endif /* NotificationManager_h */
