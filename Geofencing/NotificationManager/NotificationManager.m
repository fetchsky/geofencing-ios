//
//  NotificationManager.m
//  LocationServiceTesting
//
//  Created by Mac on 07/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PeekabooAPI.h"
#import "NotificationManager.h"

@implementation NotificationManager

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

-(void)setFirebaseInstanceId {
    [PeekabooAPI.sharedManager postInstanceId];
}
    
-(void)onNotificationReceived:(NSDictionary *)notification {
    NSDictionary *params = @{
                             @"notificationIdentifier":[notification valueForKey:@"notificationIdentifier"],
                             @"status":@"DELIVERED"
                             };
    [PeekabooAPI.sharedManager postNotificationStatus:params];
}
    
-(void)onNotificationOpened:(NSDictionary *)notification {
    NSDictionary *params = @{
                             @"notificationIdentifier":[notification valueForKey:@"notificationIdentifier"],
                             @"status":@"SEEN"
                             };
    [PeekabooAPI.sharedManager postNotificationStatus:params];
}



@end
