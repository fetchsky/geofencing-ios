//
//  Utils.m
//  Geofencing
//
//  Created by Mohammad Zain on 07/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Utils.h"

@implementation Utils


+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

+ (NSString *)getLatLongString:(NSNumber *)latitude
             longitude:(NSNumber *)longitude {
    return [[NSString alloc] initWithFormat:@"%@,%@", latitude, longitude];
}

+ (NSString *)getLocalNotificationBody:(NSDictionary *)params {
    return [[NSString alloc] initWithFormat:@"%@,%@", params[@"latitude"], params[@"longitude"]];
}

+ (void)log:(NSString *)str {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"%@", str);
    });
}

@end
