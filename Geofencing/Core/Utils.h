//
//  Utils.h
//  Geofencing
//
//  Created by Mohammad Zain on 07/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef Utils_h
#define Utils_h

@interface Utils : NSObject 

+(id)sharedManager;

+(NSString *)getLocalNotificationBody:(NSDictionary *)params;

+(NSString *)getLatLongString:(NSNumber *)latitude
                    longitude:(NSNumber *)longitude;

+(void)log:(NSString *)str;

@end




#endif /* Utils_h */
