//
//  ApplicationConfigs.h
//  Geofencing
//
//  Created by Mohammad Zain on 24/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef ApplicationConfigs_h
#define ApplicationConfigs_h


@interface ApplicationConfigs : NSObject

+(id)sharedManager;

- (NSString *) getBaseUrl;
- (void) setBaseUrl:(NSString *)url;

- (NSString *) getInstanceId;
- (void) setInstanceId:(NSString *)instanceId;

- (BOOL) showLocationLocalNotification;
- (void) setShowLocationLocalNotification:(BOOL)instanceId;

@end


#endif /* ApplicationConfigs_h */
