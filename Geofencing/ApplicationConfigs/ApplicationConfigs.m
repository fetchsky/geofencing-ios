//
//  ApplicationConfigs.m
//  Geofencing
//
//  Created by Mohammad Zain on 24/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalStorage.h"
#import "ApplicationConfigs.h"

@implementation ApplicationConfigs {
    NSString* baseUrl;
    NSString* instanceId;
    BOOL showLocalNotifications;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}


- (NSString *) getBaseUrl {
    if (!baseUrl) {
        baseUrl = [LocalStorage.sharedManager getBaseUrl];
    }
    return baseUrl;
};

- (void) setBaseUrl:(NSString *)url {
    baseUrl = url;
    [LocalStorage.sharedManager setBaseUrl:url];
}

- (NSString *) getInstanceId {
    if (!instanceId) {
        instanceId = [LocalStorage.sharedManager getInstanceId];
    }
    return instanceId;
};

- (void) setInstanceId:(NSString *)instance {
    instanceId = instance;
    [LocalStorage.sharedManager setInstanceId:instance];
};


- (BOOL) showLocationLocalNotification {
    return showLocalNotifications;
};
- (void) setShowLocationLocalNotification:(BOOL)show {
    showLocalNotifications = show;
}

@end
