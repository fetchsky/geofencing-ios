//
//  Geofencing.m
//  LocationServiceTesting
//
//  Created by Mac on 10/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Geofencing.h"
#import "LocationManager.h"
#import "NotificationManager.h"
#import "BackgroundScheduler.h"
#import "ApplicationConfigs.h"
//#import "NotificationContent.h"

@implementation Geofencing

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (instancetype) init {
    self = [super init];
    _GEOFENCING_PARAMS_BASE_URL = @"baseUrl";
    _GEOFENCING_PARAMS_INSTANCE_ID = @"instanceId";
    return self;
}

-(void)setFirebaseInstanceId:(NSDictionary *)options {
    NSLog(@"setFirebaseInstanceId %@", options);
    [ApplicationConfigs.sharedManager setInstanceId:options[@"instanceId"]];
    [ApplicationConfigs.sharedManager setBaseUrl:options[@"baseUrl"]];
    [NotificationManager.sharedManager setFirebaseInstanceId];
}

-(void)onNotificationReceived:(NSDictionary *)notification {
    if ([notification valueForKey:@"notificationIdentifier"]) {
        [NotificationManager.sharedManager onNotificationReceived:notification];
        return;
    }
}
    
-(void)onNotificationOpened:(NSDictionary *)notification {
    [NotificationManager.sharedManager onNotificationOpened:notification];
}

-(void)trackLocation:(NSDictionary *)options {
    NSLog(@"trackLocation %@", options);
    [ApplicationConfigs.sharedManager setInstanceId:options[@"instanceId"]];
    [ApplicationConfigs.sharedManager setBaseUrl:options[@"baseUrl"]];
    if (options[@"showLocationLocalNotification"]) {
        [ApplicationConfigs.sharedManager setShowLocationLocalNotification:[options[@"showLocationLocalNotification"] boolValue]];
    } else {
        [ApplicationConfigs.sharedManager setShowLocationLocalNotification:@NO];
    }
//    [LocationManager.sharedManager initializeLocationManager];startMonitoringSignificantLocationChange
//    [LocationManager.sharedManager startMonitoringSignificantLocationChange];
    [LocationManager.sharedManager startMonitoringLocation];
//    [NotificationContent.sharedManager initialize];
}

@end
