//
//  BackgroundScheduler.m
//  Geofencing
//
//  Created by Mohammad Zain on 09/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "BackgroundScheduler.h"
#import "PeekabooAPI.h"

@implementation BackgroundScheduler {
    NSString *taskIdentifier;
};


+ (id)sharedManager {
    static id sharedMyModel = nil;	
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (instancetype) init {
    self = [super init];
    taskIdentifier = @"peekaboo.geofencing.refresh";
    return self;
}


-(void)scheduleNotification:(NSDictionary *)params {
    NSLog(@"scheduleDwellInterval %@", params);
    float dwellInterval = [[params valueForKey:@"dwellInterval"] floatValue];
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"Hi" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:@"Hello" arguments:nil];
    [content setCategoryIdentifier:@"pkb_deal_navigate"];
    NSDictionary *userInfo = @{
        @"radius": [params valueForKey:@"radius"],
        @"latitude": [params valueForKey:@"latitude"],
        @"longitude": [params valueForKey:@"longitude"],
        @"dwellIntervalLapsed": @"true",
        @"source": @"PEEKABOO",
    };
    [content setUserInfo:userInfo];
    content.badge = @0;
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:30.f
                                                                                                    repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:taskIdentifier
                                                                          content:content
                                                                          trigger:trigger];
    dispatch_async(dispatch_get_main_queue(), ^{
        /// 3. schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
            }
        }];
    });
 }

-(void)cancelNotification {
    NSLog(@"cancelDwellInterval");
    UIApplication *app = [UIApplication sharedApplication];
    [app setApplicationIconBadgeNumber:[app applicationIconBadgeNumber] - 1];
    [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[taskIdentifier]];
}

-(void)onLocalNotificaitionReceive:(NSDictionary *)params {
    NSLog(@"onLocalNotificaitionReceive %@", params);
    [BackgroundScheduler.sharedManager scheduleNotification:params];
//    UIApplication *app = [UIApplication sharedApplication];
//    [app setApplicationIconBadgeNumber:[app applicationIconBadgeNumber] - 1];
//    [PeekabooAPI.sharedManager postLocation:params];
}


-(void)sendLocationLocalNotification:(NSString *)body {
    NSLog(@"sendLocationLocalNotification %@", body);
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"Location Sent" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:body arguments:nil];
    [content setCategoryIdentifier:@"pkb_deal_navigate"];
    content.badge = @1;
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10.f
                                                                                                    repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"geofencing"
                                                                          content:content
                                                                          trigger:trigger];
    dispatch_async(dispatch_get_main_queue(), ^{
        /// 3. schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
            }
        }];
    });
 }


@end
