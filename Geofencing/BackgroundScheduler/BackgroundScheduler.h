//
//  BackgroundScheduler.h
//  Geofencing
//
//  Created by Mohammad Zain on 09/10/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef BackgroundScheduler_h
#define BackgroundScheduler_h


@interface BackgroundScheduler : NSObject

+(id)sharedManager;

-(void)scheduleNotification:(NSDictionary *)params;
-(void)cancelNotification;
-(void)onLocalNotificaitionReceive:(NSDictionary *)params;
-(void)sendLocationLocalNotification:(NSString *)body;

@end

#endif /* BackgroundScheduler_h */
